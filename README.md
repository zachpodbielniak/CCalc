# CCalc

A calculator written in C, which serves as a testing point for my CLex sub-library in my PodNet library.

CLex is a full parser, that includes a lexer, tokenizer, and customizable parser.