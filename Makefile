CC = gcc
STD = -std=c89
WARNINGS = -Wall -Wextra -Wshadow -Wunsafe-loop-optimizations -Wpointer-arith
WARNINGS += -Wfloat-equal -Wswitch-enum -Wstrict-aliasing -Wno-missing-braces
WARNINGS += -Wno-cast-function-type #-Wno-switch-enum
DEFINES = -D _DEFAULT_SOURCE
DEFINES += -D _GNU_SOURCE

DEFINES_D = $(DEFINES)
DEFINES_D += -D __DEBUG__


OPTIMIZE = -Ofast -funroll-loops -fstrict-aliasing
OPTIMIZE += -fstack-protector-strong
MARCH = -march=native
MTUNE = -mtune=native

CC_FLAGS = $(STD)
CC_FLAGS += $(WARNINGS)
CC_FLAGS += $(DEFINES)
CC_FLAGS += $(OPTIMIZE)
CC_FLAGS += $(MARCH)
CC_FLAGS += $(MTUNE)

CC_FLAGS_D = $(STD)
CC_FLAGS_D += $(WARNINGS)
CC_FLAGS_D += $(DEFINES_D)

CC_FLAGS_T = $(CC_FLAGS_D)
CC_FLAGS_T += -Wno-unused-variable

ASM_FLAGS = -Wall
#ASM_FLAGS += -Ox


all: bin
	$(CC) -s -o bin/ccalc Main.c -lpodnet -lm $(CC_FLAGS)

debug: bin
	$(CC) -g -o bin/ccalc Main.c -lpodnet_d -lm $(CC_FLAGS_D)


bin:
	mkdir -p bin/

clean: 
	rm -rf bin/