#include <PodNet/PodNet.h>
#include <PodNet/CLex/CLexer.h>
#include <PodNet/CLex/CTokenTable.h>
#include <PodNet/CLex/CParser.h>
#include <PodNet/CString/CString.h>
#include <PodNet/CFile/CFile.h>
#include <ctype.h>


typedef enum __TOKEN_TYPE
{	
	TOKEN_TYPE_UNKNOWN	= 0,
	TOKEN_TYPE_ADD		= 1,
	TOKEN_TYPE_SUB		= 2,
	TOKEN_TYPE_MUL		= 3,
	TOKEN_TYPE_DIV		= 4,
	TOKEN_TYPE_POW		= 5
} TOKEN_TYPE;




typedef enum __PARSE_STATE
{
	STATE_LITERAL		= 0,
	STATE_ADD		= 1,
	STATE_SUB		= 2,
	STATE_MUL		= 3,
	STATE_DIV		= 4,
	STATE_POW		= 5,
	STATE_NEG		= 6,
	STATE_ERROR		= MAX_ULONG
} PARSE_STATE;




typedef struct __PROGRAM_STATE
{
	HANDLE 		hLexer;
	HANDLE 		hTokenTable;
	HANDLE 		hParser;
	CSTRING 	csInputBuffer[8192];
	DOUBLE 		dbState;
	DOUBLE 		dbTemp;
	TOKEN_TYPE	tfLastCommand;
	BOOL 		bStarted;
} PROGRAM_STATE, *LPPROGRAM_STATE;




LPSTR dlpszSplitters[] = {" ", "\t", "\n", "+", "-", "/", "*"};
TOKEN lptknData[] = {
	{"+", TOKEN_TYPE_ADD},
	{"-", TOKEN_TYPE_SUB},
	{"*", TOKEN_TYPE_MUL},
	{"/", TOKEN_TYPE_DIV},
	{"^", TOKEN_TYPE_POW}
};




INTERNAL_OPERATION
VOID
__ReadInput(
	_In_ 		LPPROGRAM_STATE		lpState
){
	LPSTR lpszBuffer;
	ARCHLONG alSize;

	lpszBuffer = lpState->csInputBuffer;
	ZeroMemory(lpState->csInputBuffer, sizeof(lpState->csInputBuffer));

	alSize = ReadLineFromFile(
		GetStandardIn(), 
		&lpszBuffer, 
		sizeof(lpState->csInputBuffer), 
	0);

	if (1 >= alSize)
	{ __ReadInput(lpState); }

	LexerSetText(lpState->hLexer, lpState->csInputBuffer);
}




INTERNAL_OPERATION
VOID
__Parse(
	_In_ 		LPPROGRAM_STATE		lpState
){
	CSTRING csBufferLeft[8192];
	CSTRING csBufferRight[8192];
	TOKENFLAG tfType;
	UARCHLONG ualIterator;

	lpState->bStarted = FALSE;
	lpState->tfLastCommand = 0;
	lpState->dbState = 0.0;
	lpState->dbTemp = 0.0;

	Parse(lpState->hParser);
	WaitForSingleObject(lpState->hParser, INFINITE_WAIT_TIME);
}




INTERNAL_OPERATION
MACHINESTATE
__ParserAdd(
	_In_		HANDLE 		hParser,
	_In_ 		MACHINESTATE	msCurrent,
	_In_		LEXEME 		lxLeft,
	_In_ 		LEXEME 		lxRight,
	_In_ 		TOKENFLAG 	tfType,
	_In_Opt_ 	LPVOID 		lpData
){
	UNREFERENCED_PARAMETER(hParser);
	UNREFERENCED_PARAMETER(lxLeft);
	UNREFERENCED_PARAMETER(lxRight);
	UNREFERENCED_PARAMETER(tfType);
	UNREFERENCED_PARAMETER(lpData);

	MACHINESTATE msUpdate;
	msUpdate = msCurrent;

	if (STATE_LITERAL == msCurrent)
	{ msUpdate = STATE_ADD; }

	return msUpdate;		
}




INTERNAL_OPERATION
MACHINESTATE
__ParserSub(
	_In_		HANDLE 		hParser,
	_In_ 		MACHINESTATE	msCurrent,
	_In_		LEXEME 		lxLeft,
	_In_ 		LEXEME 		lxRight,
	_In_ 		TOKENFLAG 	tfType,
	_In_Opt_ 	LPVOID 		lpData
){
	UNREFERENCED_PARAMETER(hParser);
	UNREFERENCED_PARAMETER(lxLeft);
	UNREFERENCED_PARAMETER(lxRight);
	UNREFERENCED_PARAMETER(tfType);
	UNREFERENCED_PARAMETER(lpData);

	MACHINESTATE msUpdate;
	msUpdate = msCurrent;

	if (STATE_LITERAL == msCurrent)
	{ msUpdate = STATE_SUB; }
	else /* Negate */
	{ msUpdate = STATE_NEG; }

	return msUpdate;		
}




INTERNAL_OPERATION
MACHINESTATE
__ParserMul(
	_In_		HANDLE 		hParser,
	_In_ 		MACHINESTATE	msCurrent,
	_In_		LEXEME 		lxLeft,
	_In_ 		LEXEME 		lxRight,
	_In_ 		TOKENFLAG 	tfType,
	_In_Opt_ 	LPVOID 		lpData
){
	UNREFERENCED_PARAMETER(hParser);
	UNREFERENCED_PARAMETER(lxLeft);
	UNREFERENCED_PARAMETER(lxRight);
	UNREFERENCED_PARAMETER(tfType);
	UNREFERENCED_PARAMETER(lpData);

	MACHINESTATE msUpdate;
	msUpdate = msCurrent;

	if (STATE_LITERAL == msCurrent)
	{ msUpdate = STATE_MUL; }

	return msUpdate;		
}




INTERNAL_OPERATION
MACHINESTATE
__ParserDiv(
	_In_		HANDLE 		hParser,
	_In_ 		MACHINESTATE	msCurrent,
	_In_		LEXEME 		lxLeft,
	_In_ 		LEXEME 		lxRight,
	_In_ 		TOKENFLAG 	tfType,
	_In_Opt_ 	LPVOID 		lpData
){
	UNREFERENCED_PARAMETER(hParser);
	UNREFERENCED_PARAMETER(lxLeft);
	UNREFERENCED_PARAMETER(lxRight);
	UNREFERENCED_PARAMETER(tfType);
	UNREFERENCED_PARAMETER(lpData);

	MACHINESTATE msUpdate;
	msUpdate = msCurrent;

	if (STATE_LITERAL == msCurrent)
	{ msUpdate = STATE_DIV; }

	return msUpdate;		
}




INTERNAL_OPERATION
MACHINESTATE
__ParserPow(
	_In_		HANDLE 		hParser,
	_In_ 		MACHINESTATE	msCurrent,
	_In_		LEXEME 		lxLeft,
	_In_ 		LEXEME 		lxRight,
	_In_ 		TOKENFLAG 	tfType,
	_In_Opt_ 	LPVOID 		lpData
){
	UNREFERENCED_PARAMETER(hParser);
	UNREFERENCED_PARAMETER(lxLeft);
	UNREFERENCED_PARAMETER(lxRight);
	UNREFERENCED_PARAMETER(tfType);
	UNREFERENCED_PARAMETER(lpData);

	MACHINESTATE msUpdate;
	msUpdate = msCurrent;

	if (STATE_LITERAL == msCurrent)
	{ msUpdate = STATE_POW; }

	return msUpdate;		
}




INTERNAL_OPERATION
MACHINESTATE
__ParserLiteral(
	_In_		HANDLE 		hParser,
	_In_ 		MACHINESTATE	msCurrent,
	_In_		LEXEME 		lxLeft,
	_In_ 		LEXEME 		lxRight,
	_In_ 		TOKENFLAG 	tfType,
	_In_Opt_ 	LPVOID 		lpData
){
	UNREFERENCED_PARAMETER(hParser);
	UNREFERENCED_PARAMETER(tfType);
	EXIT_IF_UNLIKELY_NULL(lxRight, msCurrent);

	MACHINESTATE msUpdate;
	LPPROGRAM_STATE lpState;
	UARCHLONG ualIterator;

	msUpdate = msCurrent;
	lpState = lpData;

	if (
		'\0' == lxLeft[0] &&
		'\0' == lxRight[0]
	){ return msCurrent; }

	StringReplaceCharacter(lxLeft, '\n', '\0');

	for (
		ualIterator = 0;
		ualIterator < StringLength(lxLeft);
		ualIterator++
	){ 
		if (!isdigit(lxLeft[ualIterator]))
		{ return msCurrent; }
	}

	switch ((UARCHLONG)msCurrent)
	{
		case STATE_ADD:
		{
			StringScanFormat(lxLeft, "%lf", &(lpState->dbTemp));
			lpState->dbState += lpState->dbTemp;			
			break;
		}

		case STATE_SUB:
		{
			StringScanFormat(lxLeft, "%lf", &(lpState->dbTemp));
			lpState->dbState -= lpState->dbTemp;
			break;
		}

		case STATE_MUL:
		{
			StringScanFormat(lxLeft, "%lf", &(lpState->dbTemp));
			lpState->dbState *= lpState->dbTemp;
			break;
		}

		case STATE_DIV:
		{
			StringScanFormat(lxLeft, "%lf", &(lpState->dbTemp));
			lpState->dbState /= lpState->dbTemp;
			break;
		}

		case STATE_POW:
		{
			StringScanFormat(lxLeft, "%lf", &(lpState->dbTemp));
			lpState->dbState = pow(lpState->dbState, lpState->dbTemp);
			break;
		}

		case STATE_NEG:
		{
			StringScanFormat(lxLeft, "%lf", &(lpState->dbTemp));
			lpState->dbTemp *= -1.0;		
			break;
		}

		case MACHINE_STATE_NULL:
		{
			StringScanFormat(lxLeft, "%lf", &(lpState->dbState));
			break;
		}
	}

	if ('\0' == lxRight[0])
	{ 
		msUpdate = MACHINE_STATE_NULL;
		PrintFormat("\t= %lf\n", lpState->dbState);
	}
	else
	{ msUpdate = STATE_LITERAL; }

	return msUpdate;		
}




INTERNAL_OPERATION
VOID
__Init(
	_In_ 		LPPROGRAM_STATE		lpState
){
	lpState->hLexer = CreateLexer(
		8192,
		dlpszSplitters,
		7
	);
	
	lpState->hTokenTable = CreateTokenTable(lptknData, 5);
	lpState->hParser = CreateParser(lpState->hLexer, lpState->hTokenTable); 
	
	ParserRegisterCallback(
		lpState->hParser,
		TOKEN_TYPE_ADD,
		__ParserAdd,
		NULLPTR
	);
	ParserRegisterCallback(
		lpState->hParser,
		TOKEN_TYPE_SUB,
		__ParserSub,
		NULLPTR
	);
	ParserRegisterCallback(
		lpState->hParser,
		TOKEN_TYPE_MUL,
		__ParserMul,
		NULLPTR
	);
	ParserRegisterCallback(
		lpState->hParser,
		TOKEN_TYPE_DIV,
		__ParserDiv,
		NULLPTR
	);
	ParserRegisterCallback(
		lpState->hParser,
		TOKEN_TYPE_POW,
		__ParserPow,
		NULLPTR
	);
	ParserRegisterCallback(
		lpState->hParser,
		TOKEN_TYPE_UNKNOWN,
		__ParserLiteral,
		lpState
	);
}




LONG 
Main(
	_In_ 		LONG 			lArgCount, 
	_In_ 		DLPSTR 			dlpszArgValues
){
	UNREFERENCED_PARAMETER(lArgCount);
	UNREFERENCED_PARAMETER(dlpszArgValues);

	PROGRAM_STATE psData;
	__Init(&psData);
	
	INFINITE_LOOP()
	{
		__ReadInput(&psData);
		__Parse(&psData);
	}

	return 0;
}